package me.ijauradunbi.anotherthing.netw

import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.getAs

open class Wut {
    /*
    POST
    http(s)://a.4cdn.org/<board>/thread/<thread_number>.json
    http(s)://a.4cdn.org/<board>/<page_number>.json (1 is main index)
    CATALOG.
    http(s)://a.4cdn.org/<board>/catalog.json
    THREADS.
    http(s)://a.4cdn.org/<board>/threads.json
    ARCHIVE.
    http(s)://a.4cdn.org/<board>/archive.json
    LIST BOARDS.
    http(s)://a.4cdn.org/boards.json
     */

    fun getPost(board: String, post: String): String {
        val url: String = "$baseUrl$board/thread/$post.json"
        Log.d(TAG, url)
        return getJson(url)
    }

    fun getCatalog(board: String): String {
        val url: String = baseUrl + board + "/catalog.json"
        Log.d(TAG, url)
        return getJson(url)
    }

    fun getThreads(board: String): String {
        val url: String = baseUrl + board + "/threads.json"
        Log.d(TAG, url)
        return getJson(url)
    }

    fun getArchive(board: String): String {
        val url: String = baseUrl + board + "/archive.json"
        Log.d(TAG, url)
        return getJson(url)
    }

    fun getBoards(): String {
        val url: String = baseUrl + "boards.json"
        Log.d(TAG, url)
        return getJson(url)
    }

    private fun getJson(url: String): String {
        val something: Triple<Request, Response, Result<String, FuelError>> = url.httpGet().responseString()
        val (req, resp, result) = something
        val json: String = when (result) {
            is Result.Failure -> "nothing"
            is Result.Success -> result.getAs<String>().toString()
        }
        return json
    }

    fun getResponse(board: String, post: String): String {
        Log.d(TAG, board + " " + post)
        if (board.equals("") && post.equals("")) {
            return getBoards()
        }
        if (post.equals("") && !board.equals("")) {
            // return getThreads(board)
            return getCatalog(board)
        }
        return getPost(board, post)
    }

    companion object {
        val TAG = Wut::class.java.name!!
        val baseUrl = "https://a.4cdn.org/"
    }
}