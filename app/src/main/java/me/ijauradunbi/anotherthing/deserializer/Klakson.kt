package me.ijauradunbi.anotherthing.deserializer

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import me.ijauradunbi.anotherthing.model.*

/**
 * Created on 08/09/2016.
 */

open class Klakson {
    val gson = Gson()
    fun jsonToPageThreads(response: String): List<PageThread> {
        val listPageThreads = gson.fromJson<List<PageThread>>(response)
        /*
        listPageThreads.map {
            Log.d(TAG, it.javaClass.canonicalName + " : " + it.toString())
        }
        */
        return listPageThreads
    }

    fun jsonToBoards(response: String) : List<Board> {
        val container = gson.fromJson<ContainerBoard>(response)
        return container.boards
    }

    fun jsonToCatalog(response: String): List<Catalog> {
        return gson.fromJson<List<Catalog>>(response)
    }

    fun jsonToPost(response: String): List<Post> {
        val container = gson.fromJson<ContainerPost>(response)
        return container.posts
    }

    companion object {
        val TAG = Klakson::class.java.canonicalName!!
    }
}
