package me.ijauradunbi.anotherthing.activity

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import me.ijauradunbi.anotherthing.R
import me.ijauradunbi.anotherthing.deserializer.Klakson
import me.ijauradunbi.anotherthing.model.Catalog
import me.ijauradunbi.anotherthing.netw.Wut
import me.ijauradunbi.anotherthing.ui.adapter.CatalogListAdapter
import me.ijauradunbi.anotherthing.ui.layout.MainActivityUI
import me.ijauradunbi.anotherthing.ui.layout.MainActivityUI.Companion.LACI_ID
import me.ijauradunbi.anotherthing.ui.layout.MainActivityUI.Companion.TOOLBAR_ID
import me.ijauradunbi.anotherthing.ui.layout.MainActivityUI.Companion.RECYCLE_ID
import org.jetbrains.anko.*
import java.util.*
import java.util.regex.Pattern

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, AnkoLogger {

    lateinit var laci: DrawerLayout
    lateinit var toolbar: Toolbar
    lateinit var rv: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUI().setContentView(this)

        laci = find<DrawerLayout>(LACI_ID)
        toolbar = find<Toolbar>(TOOLBAR_ID)
        rv = find<RecyclerView>(RECYCLE_ID)
        val toggle = ActionBarDrawerToggle(this, laci, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        laci.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var board: String = item.title.toString()
        val matcher = PATTERN.matcher(board)

        if (matcher.find()) {
            Log.d(TAG, matcher.group(0).replace("/", ""))
        }
        board = matcher.group(0).replace("/", "")
        toolbar.title =  board
        doAsync {
            val resp: String = wut.getCatalog(board)
            // val resp: String = wut.getResponse(b, p)
            // val pT = klakson.jsonToPageThreads(resp)
            // val pT: String = klakson.jsonToPageThreads(resp).toString()
            // val pT: String = klakson.jsonToPageThreads(resp).map {
            //     it.toString()
            // }.toString()
            // TODO: add different adapters depends on the user's input
            uiThread {
                // contentTextView.text = pT
                // rv.adapter = PageThreadListAdapter(context, pT as ArrayList<PageThread>)
                // val c = klakson.jsonToCatalog(resp)
                rv.adapter = CatalogListAdapter(applicationContext, klakson.jsonToCatalog(resp) as ArrayList<Catalog>)
            }
        }
        return true
    }

    companion object {
        private val TAG = MainActivity::class.java.canonicalName!!
        private val PATTERN = Pattern.compile("""/(.*?)/""")
        private val wut = Wut()
        private val klakson = Klakson()
    }
}

