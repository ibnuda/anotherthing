package me.ijauradunbi.anotherthing.ui.drawer

import android.support.design.widget.NavigationView
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.textView

/**
 * Created on 14/09/2016.
 */

class NdhasComponent : AnkoComponent<NavigationView> {

    override fun createView(ui: AnkoContext<NavigationView>): View = with(ui) {
        linearLayout {
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.BOTTOM
            textView {
                text = "Add something here."
            }
        }
    }

}
