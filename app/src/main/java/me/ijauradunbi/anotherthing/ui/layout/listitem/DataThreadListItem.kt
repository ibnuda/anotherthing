package me.ijauradunbi.anotherthing.ui.layout.listitem

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.CardView
import android.view.Gravity
import android.widget.TextView
import me.ijauradunbi.anotherthing.model.DataThread
import org.jetbrains.anko.*

/**
 * Created on 11/09/2016.
 */

class DataThreadListItem(context: Context) : CardView(context) {
    var threadNo: TextView? = null
    var lastModified: TextView? = null

    init {
        this.apply {
            relativeLayout {
                backgroundColor = Color.GRAY
                padding = 16
                minimumHeight = 10
                threadNo = textView {
                    id = 8
                    gravity = Gravity.LEFT
                }.lparams(width = matchParent, height = wrapContent) {
                    padding = 21
                }
                lastModified = textView {
                    id = 5
                    gravity = Gravity.RIGHT
                }.lparams(width = matchParent, height = wrapContent) {
                    padding = 21
                }
            }
        }
    }

    fun bind(dataThread: DataThread) {
        threadNo?.text = dataThread.no.toString()
        lastModified?.text = dataThread.last_modified.toString()
    }

    companion object {
        val TAG = DataThreadListItem::class.java.canonicalName!!
    }
}
