package me.ijauradunbi.anotherthing.ui.layout

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import me.ijauradunbi.anotherthing.R
import me.ijauradunbi.anotherthing.activity.MainActivity
import me.ijauradunbi.anotherthing.deserializer.Klakson
import me.ijauradunbi.anotherthing.model.Catalog
import me.ijauradunbi.anotherthing.netw.Wut
import me.ijauradunbi.anotherthing.ui.adapter.CatalogListAdapter
import me.ijauradunbi.anotherthing.ui.adapter.PostListAdapter
import me.ijauradunbi.anotherthing.ui.adapter.SomethingAdapter
import me.ijauradunbi.anotherthing.ui.drawer.NdhasComponent
import me.ijauradunbi.anotherthing.util.AnkoViewCompat.generateViewId
import me.ijauradunbi.anotherthing.util.colorAttr
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.navigationView
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.drawerLayout
import java.util.*

/**
 * Created on 08/09/2016.
 */
class MainActivityUI : AnkoComponent<MainActivity>, AnkoLogger {
    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        drawerLayout {
            fitsSystemWindows = true
            id = LACI_ID
            coordinatorLayout {
                appBarLayout(R.style.AppTheme_AppBarOverlay) {
                    toolbar {
                        id = TOOLBAR_ID
                        backgroundColor = colorAttr(R.attr.colorPrimary)
                    }.lparams(width = matchParent, height = wrapContent)
                }.lparams(width = matchParent, height = wrapContent)

                verticalLayout {
                    // verticalPadding = 18
                    val boardEditText: EditText = editText()
                    val postEditText: EditText = editText()
                    val checkButton: Button = button(text = "check")
                    val rv = recyclerView {
                        id = RECYCLE_ID
                        layoutManager = LinearLayoutManager(context)
                    }.lparams(width = matchParent, height = wrapContent)
                    checkButton.onClick {
                        doAsync {
                            val b: String = boardEditText.text.toString()
                            val p: String = postEditText.text.toString()
                            val resp: String = wut.getResponse(b, p)
                            uiThread {
                                rv.adapter = PostListAdapter(context, klakson.jsonToPost(resp))
                            }
                        }
                    }
                }.lparams(width = matchParent, height = wrapContent) {
                    // behavior = AppBarLayout.ScrollingViewBehavior()
                }
            }.lparams(width = matchParent, height = matchParent)

            navigationView {
                val headerContext = AnkoContext.create(ctx, this)
                val headerView = NdhasComponent()
                        .createView(headerContext)
                        .lparams(width = matchParent, height = wrapContent)
                addHeaderView(headerView)
                doAsync {
                    val boardsJson = wut.getBoards()
                    uiThread {

                        klakson.jsonToBoards(boardsJson).map {
                            menu.add('/' + it.board + '/' + " : " + it.title)
                        }
                    }
                }
                if (!isInEditMode) {
                    setNavigationItemSelectedListener(ui.owner)
                }
            }.lparams(height = matchParent) {
                gravity = Gravity.START
            }

            if (isInEditMode) {
                openDrawer(Gravity.START)
            }
        }
    }

    fun something(context: Context, string: String): SomethingAdapter {
        return CatalogListAdapter(context, Klakson().jsonToCatalog(string) as ArrayList<Catalog>)
    }

    companion object {
        val TAG = MainActivityUI::class.java.name!!
        val TOOLBAR_ID = generateViewId()
        val LACI_ID = generateViewId()
        val RECYCLE_ID = generateViewId()
        val wut = Wut()
        val klakson = Klakson()
    }
}