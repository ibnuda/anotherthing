package me.ijauradunbi.anotherthing.ui.layout

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import me.ijauradunbi.anotherthing.R
import me.ijauradunbi.anotherthing.fragment.PageThreadFragment
import me.ijauradunbi.anotherthing.model.PageThread
import me.ijauradunbi.anotherthing.ui.adapter.PageThreadListAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import java.util.*

/**
 * Created on 11/09/2016.
 */

class PageThreadFragmentUI(val context: Context) : AnkoComponent<PageThreadFragment> {
    lateinit var ankoContext: AnkoContext<PageThreadFragment>
    lateinit var contentLayout: RelativeLayout
    lateinit var threadList: RecyclerView
    lateinit var loadingLayout: ProgressBar

    override fun createView(ui: AnkoContext<PageThreadFragment>): View = with(ui) {
        ankoContext = ui
        return coordinatorLayout {
            appBarLayout {
                toolbar {
                    title = context.getString(R.string.app_name)
                }.lparams(width = matchParent)
            }.lparams(width = matchParent)

            contentLayout = relativeLayout {
                threadList = recyclerView {
                    id = 2
                    layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
                }.lparams(width = matchParent, height = wrapContent)
            }
        }
    }

    fun populateThreadList(threads: List<PageThread>) {
        threadList.adapter = PageThreadListAdapter(context = context, pageThreadList = threads as ArrayList<PageThread>)
    }

}
