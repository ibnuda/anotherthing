package me.ijauradunbi.anotherthing.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.anotherthing.model.DataThread
import me.ijauradunbi.anotherthing.ui.layout.listitem.DataThreadListItem
import java.util.*

/**
 * Created on 11/09/2016.
 */

class DataThreadListAdapter(val context: Context, val dataThreadList: ArrayList<DataThread> = ArrayList<DataThread>())
: RecyclerView.Adapter<DataThreadListAdapter.Holder>() {
    override fun getItemCount(): Int {
        return dataThreadList.size
    }

    override fun onBindViewHolder(holder: Holder?, position: Int) {
        holder!!.bind(dataThreadList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        return Holder(DataThreadListItem(context))
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(dataThread: DataThread) {
            (itemView as DataThreadListItem).bind(dataThread)
        }
    }
}
