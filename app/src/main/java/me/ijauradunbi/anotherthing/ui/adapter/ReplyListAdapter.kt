package me.ijauradunbi.anotherthing.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.anotherthing.model.Reply
import me.ijauradunbi.anotherthing.ui.layout.listitem.ReplyListItem
import java.util.*

class ReplyListAdapter(val context: Context, val replies: ArrayList<Reply>) : RecyclerView.Adapter<ReplyListAdapter.Holder>() {
    override fun onBindViewHolder(holder: Holder?, position: Int) {
        holder?.bind(replies[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        return Holder(ReplyListItem(context))
    }

    override fun getItemCount(): Int {
        return replies.count()
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(reply: Reply) {
            if (itemView is ReplyListItem) {
                itemView.bind(reply)
            }
        }
    }
}
