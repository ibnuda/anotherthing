package me.ijauradunbi.anotherthing.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.anotherthing.model.PageThread
import me.ijauradunbi.anotherthing.ui.layout.listitem.PageThreadListItem
import java.util.*

class PageThreadListAdapter(val context: Context, val pageThreadList: ArrayList<PageThread> = ArrayList<PageThread>())
: RecyclerView.Adapter<PageThreadListAdapter.Holder>() {
    override fun getItemCount(): Int {
        return pageThreadList.count()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(pageThreadList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        return Holder(PageThreadListItem(context))
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(pageThread: PageThread) {
            (itemView as PageThreadListItem).bind(pageThread)
        }
    }
}
