package me.ijauradunbi.anotherthing.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.anotherthing.model.Thread
import me.ijauradunbi.anotherthing.ui.layout.listitem.ThreadListItem
import java.util.*

class ThreadListAdapter(val context: Context, val threadList: ArrayList<Thread>?) : RecyclerView.Adapter<ThreadListAdapter.Holder>() {
    override fun getItemCount(): Int {
        return threadList!!.size
    }

    override fun onBindViewHolder(holder: Holder?, position: Int) {
        holder?.bind(threadList!![position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        return Holder(ThreadListItem(context))
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(thread: Thread) {
            (itemView as ThreadListItem).bind(thread)
        }
    }
}
