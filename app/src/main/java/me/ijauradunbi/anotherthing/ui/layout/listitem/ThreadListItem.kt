package me.ijauradunbi.anotherthing.ui.layout.listitem

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import me.ijauradunbi.anotherthing.model.Reply
import me.ijauradunbi.anotherthing.model.Thread
import me.ijauradunbi.anotherthing.ui.adapter.ReplyListAdapter
import me.ijauradunbi.anotherthing.util.ParseAndShit
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import java.util.*

class ThreadListItem(context: Context) : CardView(context) {
    var textNo: TextView? = null
    var textCom: TextView? = null
    var repliesPlacement: RecyclerView? = null

    init {
        relativeLayout {
            relativeLayout {
                id = 6
                textNo = textView {
                    id = 1
                }.lparams(width = matchParent, height = wrapContent)
                textCom = textView {
                    id = 4
                }.lparams(width = matchParent, height = wrapContent) {
                    below(1)
                }
            }.lparams { width = matchParent; height = wrapContent }
            linearLayout {
                repliesPlacement = recyclerView {
                    id = 4
                    layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
                }.lparams(width = matchParent, height = wrapContent)
            }.lparams {
                below(6)
            }
        }
    }

    fun bind(thread: Thread) {
        textNo?.text = thread.no.toString()
        if (thread.com != null) {
            textCom?.text = parseAndShit.parseToShit(thread.com!!)
        }
        val replies = thread.replies
        val rep = replies!!
        if (rep > 0) {
            repliesPlacement?.adapter = ReplyListAdapter(context, thread.last_replies as ArrayList<Reply>)
        }
    }

    companion object {
        val TAG = ThreadListItem::class.java.canonicalName!!
        val parseAndShit = ParseAndShit()
    }
}
