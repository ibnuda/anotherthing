package me.ijauradunbi.anotherthing.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.anotherthing.model.Post
import me.ijauradunbi.anotherthing.ui.layout.listitem.PostListItem


class PostListAdapter(val context: Context, val posts: List<Post>) : RecyclerView.Adapter<PostListAdapter.Holder>() {
    override fun onBindViewHolder(holder: Holder?, position: Int) {
        holder?.bind(posts[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        return Holder(PostListItem(context))
    }

    override fun getItemCount(): Int {
        return posts.count()
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(post: Post) {
            (itemView as PostListItem).bind(post)
        }
    }
}
