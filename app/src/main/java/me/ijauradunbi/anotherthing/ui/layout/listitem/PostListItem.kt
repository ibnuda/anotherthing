package me.ijauradunbi.anotherthing.ui.layout.listitem

import android.content.Context
import android.support.v7.widget.CardView
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import me.ijauradunbi.anotherthing.model.Post
import me.ijauradunbi.anotherthing.util.ParseAndShit
import org.jetbrains.anko.*

class PostListItem(context: Context) : CardView(context) {

    var no: TextView? = null
    var idText: TextView? = null
    var com: TextView? = null
    var filename: TextView? = null
    var now: TextView? = null
    var name: TextView? = null

    init {
        this.apply {
            relativeLayout {
                linearLayout {
                    id = 1
                    orientation = LinearLayout.HORIZONTAL
                    no = textView {
                        id = 2
                        gravity = Gravity.LEFT
                    }
                    idText = textView {
                        id = 3
                        gravity = Gravity.RIGHT
                    }
                }.lparams(height = wrapContent, width = matchParent)
                now = textView { id = 2 }.lparams { below(1) }
                linearLayout {
                    orientation = LinearLayout.HORIZONTAL
                    id = 4
                    filename = textView {
                        id = 5
                    }
                    com = textView {
                        id = 6
                    }
                }.lparams(height = wrapContent, width = matchParent) {
                    below(2)
                }
            }
        }
    }

    fun bind(post: Post) {
        no?.text = post.no.toString()
        idText?.text = post.id
        com?.text = if (!post.com.isNullOrBlank()) {
            parseAndShit.parseToShit(post.com.toString())
        } else {
            ""
        }
        now?.text = post.now
        name?.text = post.name
    }

    companion object {
        val TAG = PostListItem::class.java.canonicalName!!
        val parseAndShit = ParseAndShit()
    }
}
