package me.ijauradunbi.anotherthing.ui.layout.listitem

import android.R
import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import me.ijauradunbi.anotherthing.model.DataThread
import me.ijauradunbi.anotherthing.model.PageThread
import me.ijauradunbi.anotherthing.ui.adapter.DataThreadListAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import java.util.*

/**
 * Created on 08/09/2016.
 */

class PageThreadListItem(context: Context) : CardView(context) {
    var page: TextView? = null
    var threadsPlacement: RecyclerView? = RecyclerView(context)

    init {
        this.apply {
            layoutParams = ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            setCardBackgroundColor(R.color.white)

            relativeLayout {
                minimumHeight = dip(64)
                linearLayout {
                    id = 6
                    page = textView {
                        id = 1
                    }.lparams(width = matchParent, height = wrapContent)
                }.lparams {
                    padding = 6
                }
                linearLayout {
                    threadsPlacement = recyclerView {
                        padding = 14
                        id = 4
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
                    }.lparams(width = matchParent, height = wrapContent)
                }.lparams {
                    below(6)
                    padding = 26
                }
            }
        }
    }

    fun bind(pageThread: PageThread) {
        threadsPlacement!!.adapter = DataThreadListAdapter(context, pageThread.threads as ArrayList<DataThread>)
        page!!.text = pageThread.page.toString()
    }

    companion object {
        val TAG = PageThreadListItem::class.java.canonicalName!!
    }
}
