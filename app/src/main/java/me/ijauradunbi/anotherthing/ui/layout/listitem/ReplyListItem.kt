package me.ijauradunbi.anotherthing.ui.layout.listitem

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.CardView
import android.widget.TextView
import me.ijauradunbi.anotherthing.model.Reply
import me.ijauradunbi.anotherthing.util.ParseAndShit
import org.jetbrains.anko.*

class ReplyListItem(context: Context) : CardView(context) {

    var textNo: TextView? = null
    var textCom: TextView? = null

    init {
        this.apply {
            relativeLayout {
                padding = 16
                textNo = textView {
                    backgroundColor = Color.WHITE
                    id = 8
                }.lparams(width = matchParent, height = wrapContent)
                textCom = textView {
                    backgroundColor = Color.WHITE
                    textColor = Color.BLACK
                    id = 5
                }.lparams(width = matchParent, height = wrapContent) {
                    below(8)
                }
            }
        }
    }

    fun bind(reply: Reply) {
        textNo?.text = reply.no.toString()
        if (reply.com != null) {
            val comment = parseAndShit.parseToShit(reply.com)
            textCom?.text = comment
        }
    }

    companion object {
        val TAG = ReplyListItem::class.java.canonicalName!!
        val parseAndShit = ParseAndShit()
    }
}
