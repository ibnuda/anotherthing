package me.ijauradunbi.anotherthing.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.anotherthing.model.Catalog
import me.ijauradunbi.anotherthing.ui.layout.listitem.CatalogListItem
import java.util.*

class CatalogListAdapter(val context: Context, val catalogs: ArrayList<Catalog>) : RecyclerView.Adapter<CatalogListAdapter.Holder>(), SomethingAdapter {
    override fun onBindViewHolder(holder: Holder?, position: Int) {
        holder?.bind(catalogs[position])
    }

    override fun getItemCount(): Int {
        return catalogs.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        return Holder(CatalogListItem(context))
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(catalog: Catalog) {
            (itemView as CatalogListItem).bind(catalog)
        }
    }

}
