package me.ijauradunbi.anotherthing.ui.drawer

import android.view.Gravity
import android.view.View
import me.ijauradunbi.anotherthing.R
import me.ijauradunbi.anotherthing.activity.MainActivity
import me.ijauradunbi.anotherthing.util.AnkoViewCompat.generateViewId
import me.ijauradunbi.anotherthing.util.colorAttr
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.navigationView
import org.jetbrains.anko.support.v4.drawerLayout

/**
 * Created on 14/09/2016.
 */

class LaciComponent : AnkoComponent<MainActivity>, AnkoLogger {

    companion object {
        val TOOLBAR_ID = generateViewId()
        val DRAWER_ID = generateViewId()
    }

    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui) {
        drawerLayout {
            id = DRAWER_ID

            coordinatorLayout {
                appBarLayout {
                    toolbar {
                        id = TOOLBAR_ID
                        backgroundColor = colorAttr(R.attr.colorPrimary)
                    }.lparams(width = matchParent)
                }.lparams(width = matchParent)

                relativeLayout {
                    textView("Mungkin untuk tempat thread atau sejenisnya.")
                }
            }.lparams(width = matchParent, height = matchParent)

            navigationView {
                val headerContext = AnkoContext.create(ctx, this)
                val headerView = NdhasComponent()
                        .createView(headerContext)
                        .lparams(width = matchParent, height = wrapContent)
                addHeaderView(headerView)
                // TODO : Add menu. Maybe from 4chins/boards.
                // inflateMenu(menu thingy)
                if (!isInEditMode) {
                    setNavigationItemSelectedListener(ui.owner)
                }
            }.lparams(height = matchParent) {
                gravity = Gravity.START
            }

            if (isInEditMode) {
                openDrawer(Gravity.START)
            }
        }
    }
}
