package me.ijauradunbi.anotherthing.ui.layout.listitem

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import me.ijauradunbi.anotherthing.model.Catalog
import me.ijauradunbi.anotherthing.model.Thread
import me.ijauradunbi.anotherthing.ui.adapter.ThreadListAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import java.util.*

class CatalogListItem(context: Context) : CardView(context) {

    var page: TextView? = null
    var threadsPlacement: RecyclerView? = null

    init {
        this.apply {
            relativeLayout {
                padding = 20
                linearLayout {
                    page?.lparams(width = matchParent, height = wrapContent)
                    threadsPlacement = recyclerView {
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
                    }
                }
            }
        }
    }

    fun bind(catalog: Catalog) {
        page?.text = catalog.page.toString()
        threadsPlacement?.adapter = ThreadListAdapter(context, catalog.threads as ArrayList<Thread>)
    }

    companion object {
        val TAG = CatalogListItem::class.java.canonicalName!!
    }
}
