package me.ijauradunbi.anotherthing.util

import android.graphics.Typeface
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.StrikethroughSpan
import android.text.style.StyleSpan
import org.jsoup.Jsoup
import org.jsoup.helper.StringUtil
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.nodes.Node
import org.jsoup.nodes.TextNode
import java.util.regex.Pattern

class ParseAndShit {
    fun parseToShit(rawComment: String): CharSequence {
        val comment: String = rawComment.replace("<wbr>", "")
        val document: Document = Jsoup.parseBodyFragment(comment)
        val nodes: List<Node> = document.body().childNodes()
        val finally = nodes.map {
            nodeParse(it)
        }.reduce { a: CharSequence, b: CharSequence -> a.toString() + b.toString() }
        return finally
    }

    fun nodeParse(node: Node): CharSequence {
        if (node is TextNode) {
            val text = node.text()
            return SpannableString(text)
        } else {
            when (node.nodeName()) {
                "br" -> return "\n"
                "span" -> {
                    val span = node as Element
                    val quote: SpannableString
                    val classes = span.classNames()
                    if (classes.contains("deadlink")) {
                        quote = SpannableString(span.text())
                        quote.setSpan(StrikethroughSpan(), 0, quote.length, 0)
                    } else if (classes.contains("fortune")) {
                        quote = SpannableString("\n\n" + span.text())
                        var style = span.attr("style")
                        if (!TextUtils.isEmpty(style)) {
                            style = style.replace(" ", "")

                            val matcher = colorPattern.matcher(style)

                            var hexColor = 0xff0000
                            if (matcher.find()) {
                                val group = matcher.group(1)
                                if (!TextUtils.isEmpty(group)) {
                                    try {
                                        hexColor = Integer.parseInt(group, 16)
                                    } catch (ignored: NumberFormatException) {
                                    }

                                }
                            }

                            if (hexColor >= 0 && hexColor <= 0xffffff) {
                                quote.setSpan(StyleSpan(Typeface.BOLD), 0, quote.length, 0)
                            }
                        }
                    } else if (classes.contains("abbr")) {
                        return ""
                    } else {
                        quote = SpannableString(span.text())
                    }
                    return quote
                }
                else -> {
                    if (node is Element) {
                        return node.text()
                    } else {
                        return ""
                    }
                }
            }
        }
    }

    companion object {
        private val TAG = "ChanParser"
        private val colorPattern = Pattern.compile("color:#([0-9a-fA-F]*)")

        private fun lastCharIsWhitespace(sb: StringBuilder): Boolean {
            return sb.length != 0 && sb[sb.length - 1] == ' '
        }

        private fun appendNormalisedText(accum: StringBuilder, textNode: TextNode) {
            var text = textNode.wholeText

            if (!preserveWhitespace(textNode.parent())) {
                text = normaliseWhitespace(text)
                if (lastCharIsWhitespace(accum))
                    text = stripLeadingWhitespace(text)
            }
            accum.append(text)
        }

        private fun normaliseWhitespace(text: String): String {
            return StringUtil.normaliseWhitespace(text)
        }

        private fun stripLeadingWhitespace(text: String): String {
            return text.replaceFirst("^\\s+".toRegex(), "")
        }

        private fun preserveWhitespace(node: Node?): Boolean {
            if (node != null && node is Element) {
                return node.tag().preserveWhitespace() || node.parent() != null && node.parent().tag().preserveWhitespace()
            }
            return false
        }
    }
}
