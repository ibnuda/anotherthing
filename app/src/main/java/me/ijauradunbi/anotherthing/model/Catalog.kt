package me.ijauradunbi.anotherthing.model

data class Catalog(
        val page: Int,
        val threads: List<Thread>
)
