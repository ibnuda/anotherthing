package me.ijauradunbi.anotherthing.model

/**
 * Created on 08/09/2016.
 */
data class DataThread(
        val no: Int,
        val last_modified: Int
) {
    override fun toString(): String {
        return "thread no : ${this.no}, last_modified: ${this.last_modified}\n"
    }
}
