package me.ijauradunbi.anotherthing.model

/**
 * Created on 08/09/2016.
 */

data class PageThread(
        val page: Int,
        val threads: List<DataThread>
) {
    override fun toString(): String {
        val threads: List<String> = this.threads.map { it.toString() }
        return "page ${this.page}, thread : " + threads.toString()
    }
}
