package me.ijauradunbi.anotherthing.model

data class Board(
        val board: String,
        val title: String,
        val ws_board: Int,
        val per_page: Int,
        val pages: Int,
        val max_filesize: Int,
        val max_webm_filesize: Int,
        val max_comment_chars: Int,
        val max_webm_duration: Int,
        val bump_limit: Int,
        val cooldown: Cooldown,
        val meta_description: String,
        val spoilers: Int,
        val custom_spoilers: Int,
        val is_archived: Int
)

