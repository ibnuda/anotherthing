package me.ijauradunbi.anotherthing.model

import java.util.*


data class ContainerPost(
        val posts: ArrayList<Post>
)

data class ContainerBoard(
        val boards: ArrayList<Board>
)
