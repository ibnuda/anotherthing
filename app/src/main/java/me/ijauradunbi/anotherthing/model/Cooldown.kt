package me.ijauradunbi.anotherthing.model

data class Cooldown(
        val threads: Int,
        val replies: Int,
        val images: Int,
        val replies_intra: Int,
        val images_intra: Int
)