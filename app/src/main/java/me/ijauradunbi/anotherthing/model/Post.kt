package me.ijauradunbi.anotherthing.model

import java.util.*

/**
 * Created on 07/09/2016.
 */

data class Post(
        val no: Int,
        val id: String,
        val sticky: Int?,
        val closed: Int?,
        val now: String,
        val name: String,
        val com: String?,
        val filename: String?,
        val extension: String?,
        val w: Int?,
        val h: Int?,
        val tn_w: Int?,
        val tn_h: Int?,
        val tim: Long,
        val time: Int,
        val md5: String?,
        val fsize: Int?,
        val resto: Int?,
        val capcode: String?,
        val semantic_url: String?,
        val replies: Int?,
        val images: Int?,
        val unique_ips: Int?,
        val country_name: String?,
        val country: String?,
        val bumplimit: Int?,
        val postlimit: Int?
)
