package me.ijauradunbi.anotherthing.model

data class Reply(
        val no: Int,
        val now: String,
        val com: String?,
        val filename: String?,
        val ext: String?,
        val w: Int?,
        val h: Int?,
        val tn_w: Int?,
        val tn_h: Int?,
        val time: Int,
        val tim: Long?,
        val md5: String?,
        val filesize: Int?,
        val resto: Int?
)
